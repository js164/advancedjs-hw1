// 1. Прототипное наследование это возможность одалживать медоты и свойства у других обьектов, чтобы не пысать повторно для каждого.
// 2. Чтобы вызвать функции родителя и получать this от родительского обьекта.


class Employee {
    constructor(name, age, salary) {
        this.name = name;
        this.age = age;
        this.salary = salary

    }
    set name(name) {
        this._name = name
    }

    get name() {
        return this._name
    }

    set age(age) {
        this._age = age
    }

    get age() {
        return this._age
    }

    set salary(salary) {
        this._salary = salary
    }

    get salary() {
        return this._salary
    }
}

console.log(Employee)

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this.lang = lang
    }
    set salary(salary) {
        this._salary = salary*3
    }

    get salary() {
        return this._salary
    }

}

const alex = new Programmer('Alex', 29, 1000, ['EN, RU, UA'])
const tina = new Programmer('Tina', 25, 3000, ['EN, UA'])
const andy = new Programmer('Andy', 22, 2000, ['EN, GE, UA'])
console.log(alex)
console.log(tina)
console.log(andy)












